{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE LambdaCase, BlockArguments #-}
{-# LANGUAGE GADTs, FlexibleContexts, TypeOperators, DataKinds, PolyKinds #-}

module Lib
  ( someFunc
  )
where

import           Control.Monad
import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Vector ( Vector )
import qualified Data.Vector as V
import           Numeric.Natural
import           Polysemy
import           Polysemy.State

data HeapTerm
  = Struct Natural
  | Ref Natural
  | Arity Natural
    deriving (Show, Eq)

newtype Heap = Heap (Vector HeapTerm) deriving (Show)

data TermBuildSt
  = TermBuildSt
      (Map String Natural)
      -- ^ Used to track which holes already exist on the heap
      Heap
      -- ^ The heap being built

data TermBuild m a where
  MkRef :: String -> TermBuild m ()
  MkStruct :: String -> [m ()] -> TermBuild m ()

makeSem ''TermBuild

runTermBuild :: (Member (State TermBuildSt) r) => Sem (TermBuild ': r) a -> Sem r a
runTermBuild = interpretH $ \case

  -- When a term is encountered, add check if it
  -- already exists on the heap before setting
  -- the ref index
  MkRef r -> do
    modify $ \(TermBuildSt refs (Heap h)) ->
      case Map.lookup r refs of
        Just i -> 
          let h' = V.snoc h (Ref i)
          in TermBuildSt refs (Heap h')
        Nothing ->
          let i = fromIntegral $ V.length h + 1
              refs' = Map.insert r i refs
              h' = V.snoc h (Ref i)
          in TermBuildSt refs' (Heap h')
    pureT ()

  -- When a struct is encountered, push the
  -- struct and the arity onto the heap before
  -- processing the rest of the struct items
  MkStruct _ xs -> do
    actions <- sequence $ runT <$> xs
    modify $ \(TermBuildSt refs (Heap h)) ->
      let structHead = V.fromList [ Struct (fromIntegral $ V.length h + 1)
                                  , Arity (fromIntegral $ length xs)
                                  ]
      in TermBuildSt refs (Heap (V.concat [h, structHead]))
    void $ sequence $ raise . runTermBuild <$> actions
    pureT ()

defaultTermBuildSt :: TermBuildSt
defaultTermBuildSt = TermBuildSt mempty (Heap mempty)

query :: Sem '[ TermBuild, State TermBuildSt ] () -> Heap
query m =
  let (TermBuildSt _ heap, _) =
        run
        . runState @TermBuildSt defaultTermBuildSt
        . runTermBuild $ m
  in heap

deref :: (Member (State TermBuildSt) r) => Natural -> Sem r (HeapTerm)
deref a = do
  (TermBuildSt _ (Heap h)) <- get
  case h V.!? (fromIntegral a) of
    Just (Ref i) ->
      if i /= a then deref i else pure (Ref a)
    Just other -> pure other
    Nothing -> undefined

test :: (Member TermBuild r) => Sem r ()
test = do
  let z = mkRef "Z"
  let w = mkRef "W"
  let p = mkStruct "p"
  let h = mkStruct "h"
  let f = mkStruct "f"
  p [z, h [z, w], f [w]]

someFunc :: IO ()
someFunc = putStrLn "someFunc"
